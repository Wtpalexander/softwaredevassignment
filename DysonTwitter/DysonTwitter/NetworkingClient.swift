//
//  NetworkingClient.swift
//  DysonTwitter
//
//  Created by James Olrog on 29/01/2020.
//  Copyright © 2020 Will T P Alexander. All rights reserved.
//

import Foundation
import Alamofire
import OhhAuth
import OAuthSwift

protocol NetworkDelegate{
    func jsonReceived(json: Data)
}

class NetworkingClient {
    
    var oauthSwift: OAuth1Swift
        
    init() {
        // create an instance and retain it
        oauthSwift = OAuth1Swift(
            consumerKey:    "6gqt94LkqArkZYJ1HyKgTCNQk",
            consumerSecret: "TC4JDhzditSqcMrkiBe4hDnbNVUIGlHYFXL7Vx7phiZcnV4Z8X",
            requestTokenUrl: "https://api.twitter.com/oauth/request_token",
            authorizeUrl:    "https://api.twitter.com/oauth/authorize",
            accessTokenUrl:  "https://api.twitter.com/oauth/access_token"
        )
        
        authorize()
    }

    func authorize() {
        // authorize
        oauthSwift.authorize(
            withCallbackURL: URL(string: "oauth-swift://oauth-callback/twitter")!) { result in
            switch result {
            case .success(let (credential, _, parameters)):
              print(credential.oauthToken)
              print(credential.oauthTokenSecret)
              print(parameters["user_id"] ?? "Error")
            // Do your request
            case .failure(let error):
              print(error.localizedDescription)
            }
        }
    }
    
    func getPosts(username: String, count: Int, completion: @escaping (_ tweets: [Tweet]) -> Void) {
        
        let url = "https://api.twitter.com/1.1/statuses/user_timeline.json"
        
        //Filtering Paramters (to be addd to header of packet)
        let paras = [
            "screen_name": username,
            "count": count,
            "exclude_replies": true,
            "trim_user": true,
            "include_rts": true
            ] as [String : Any]
        
        //https://stackoverflow.com/questions/47130174/cannot-downcast-from-any-to-a-more-optional-type-string-anyobject
        oauthSwift.client.request(url, method: .GET, parameters: paras, headers: [:], completionHandler: { result in
            switch result {
                case .success(let response):
                    print(response)
                    let str = String(decoding: response.data, as: UTF8.self)
                    print(str)
                    if var tweets = try? JSONDecoder().decode([Tweet].self, from: response.data) {
                        //Sort the tweets to appear as the most recent at the beggining of the array
                        tweets.sort(by: { $0.asDate().compare($1.asDate()) == .orderedDescending })
                        
                        completion(tweets)
                        print(tweets.count)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
            }
        })
    }
}
