//
//  Report.swift
//  DysonTwitter
//
//  Created by James Olrog on 13/02/2020.
//  Copyright © 2020 Will T P Alexander. All rights reserved.
//

import Foundation

struct Report: Codable {
    var username: String
    var tweets: [Tweet]
    var dateCreated: Date
}
