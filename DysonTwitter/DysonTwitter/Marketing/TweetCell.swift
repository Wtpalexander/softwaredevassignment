//
//  TweetCell.swift
//  DysonTwitter
//
//  Created by Will T P Alexander on 29/01/2020.
//  Copyright © 2020 Will T P Alexander. All rights reserved.
//

import UIKit

class TweetCell: UITableViewCell {
    
    @IBOutlet weak var tweetTextLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var retweetsLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    
}
