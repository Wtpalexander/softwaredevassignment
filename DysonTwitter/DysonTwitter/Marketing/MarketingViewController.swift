//
//  MarketingViewController.swift
//  DysonTwitter
//
//  Created by Will T P Alexander on 29/01/2020.
//  Copyright © 2020 Will T P Alexander. All rights reserved.
//

import UIKit

class MarketingViewController: UIViewController {
    
    let networkingClient = NetworkingClient()
    
    @IBOutlet weak var accountNameTextField: UITextField!
    @IBOutlet weak var goButton: UIButton!
    @IBOutlet weak var generateReportButton: UIButton!
    @IBOutlet weak var tweetTableView: UITableView!
    
    var username: String = ""
    var tweets: [Tweet] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        //Round the buttons
        goButton.layer.cornerRadius = goButton.frame.height/2
        generateReportButton.layer.cornerRadius = generateReportButton.frame.height/2

    }
    
    @IBAction func goTapped(_ sender: Any) {
        tweetTableView.becomeFirstResponder()
        
        guard let newUsername = accountNameTextField.text else {
            return
        }
        username = newUsername
        
        //TODO: Start network request for certain tweets
        networkingClient.getPosts(username: username, count: 200, completion: { newTweets in
            self.tweets = newTweets
            self.tweetTableView.reloadData()
        })
    }
    
    @IBAction func generateReportTapped(_ sender: Any) {
            //Potentially run the request in here? Saves pressing GO
            if !tweets.isEmpty{
                performSegue(withIdentifier: "generateReport", sender: self)
            } else {
                let alert = UIAlertController(title: "Alert", message: "No tweets available for report generation", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let reportVC = segue.destination as! ReportViewController
        reportVC.username = self.username
        reportVC.tweets = self.tweets
    }

}

extension MarketingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //TODO: Work out how many tweets to show
        return tweets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tweetCell") as! TweetCell
        //TODO: Fill in the cell with info
        
        let tweet = tweets[indexPath.row]
        
        cell.tweetTextLabel?.text = tweet.text
        cell.dateTimeLabel.text = tweet.created_at
        cell.likesLabel.text = "\(tweet.favorite_count)"
        cell.retweetsLabel.text = "\(tweet.retweet_count)"
        
        return cell
    }

}
