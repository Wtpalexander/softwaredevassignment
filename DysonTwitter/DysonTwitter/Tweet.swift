//
//  Tweet.swift
//  DysonTwitter
//
//  Created by Will T P Alexander on 29/01/2020.
//  Copyright © 2020 Will T P Alexander. All rights reserved.
//

import Foundation

class Tweet: Codable {
    
    init(date: String, text: String, favoriteCount: Int, retweetCount: Int) {
        self.created_at = date
        self.id = nil
        self.text = text
        self.truncated = nil
        self.favorite_count = favoriteCount
        self.retweet_count = retweetCount
    }
    
    var created_at: String
    var id: Int?
    var text: String
    var truncated: Bool?
    
    var favorite_count: Int
    var retweet_count: Int
    
    func asDate() -> Date {
        //"Tue Jan 21 11:00:02 +0000
        let formatter = DateFormatter()
        formatter.dateFormat = "E MMM d HH:mm:ss Z yyyy"
        if let date = formatter.date(from: created_at) {
          return date
        } else {
            return Date()
        }
    }

}

