//
//  ManagementViewController.swift
//  DysonTwitter
//
//  Created by Will T P Alexander on 29/01/2020.
//  Copyright © 2020 Will T P Alexander. All rights reserved.
//

import UIKit

class ManagementViewController: UIViewController {
    
    //Variables
    var reports: [Report] = []
    var selectedReport: Report? = nil
    
    //References
    let defaults = UserDefaults.standard
    let decoder = JSONDecoder(); let encoder = JSONEncoder()
    @IBOutlet weak var reportTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reportTableView.dataSource = self
        reportTableView.delegate = self
        
        getReports()
    }
    
    func getReports() {
        if let data = defaults.data(forKey: "savedReports") {
            do {
                let decodedData = try decoder.decode([Report].self, from: data)
                reports = decodedData
                reportTableView.reloadData()
            } catch {
                print("Failed to decode JSON")
            }
        }
    }
    
    func saveReports() {
        do {
            let dataToSave = try encoder.encode(reports)
            defaults.set(dataToSave, forKey:"savedReports")
        } catch {
            print("Error saving reports")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let newVC = segue.destination as? ReportViewController {
            newVC.tweets = selectedReport?.tweets ?? []
            newVC.username = selectedReport?.username ?? ""
            newVC.isSaveButtonHidden = true
        }
        
    }
}

extension ManagementViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportCell") as! ReportCell
        
        cell.usernameLabel.text = "@\(reports[indexPath.row].username)"
        cell.tweetNumberLabel.text = "\(reports[indexPath.row].tweets.count) Tweets"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a MMM d yyyy"
        
        cell.dateCreatedLabel.text = "Date created: \(dateFormatter.string(from: reports[indexPath.row].dateCreated))"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedReport = reports[indexPath.row]
        
        self.performSegue(withIdentifier: "goToReportViewer", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            reports.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            saveReports()
        }
    }
}
