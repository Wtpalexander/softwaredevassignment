//
//  ReportCell.swift
//  DysonTwitter
//
//  Created by Will T P Alexander on 27/02/2020.
//  Copyright © 2020 Will T P Alexander. All rights reserved.
//

import UIKit

class ReportCell: UITableViewCell {
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var tweetNumberLabel: UILabel!
    @IBOutlet weak var dateCreatedLabel: UILabel!
    
}

