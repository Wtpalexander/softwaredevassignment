//
//  ScrollableGraphProvider.swift
//  DysonTwitter
//
//  Created by James Olrog on 13/02/2020.
//  Copyright © 2020 Will T P Alexander. All rights reserved.
//

import Foundation
import ScrollableGraphView

//Tidy up this class and use some variables for things like line width or animationDuration!!!

class ScrollableGraphProvider {
    
    private var tweets = [Tweet]()
    var graphs = [ScrollableGraph]()
    
    init(tweets: [Tweet]){
        self.tweets = tweets
    }
    
    func average(_ input: [Double]) -> Double {
        let average = input.reduce(0, +) / Double(input.count)
        return round(10*average)/10
    }
    
    func multiply(_ a: [Double], _ b: [Double]) -> [Double] {
        return zip(a,b).map(*)
    }
    
    func linearRegression(_ ys: [Double]) -> (((Double) -> Double), Double) {
        let xs = (1...tweets.count).map { Double($0) }
        
        let sum1 = average(multiply(ys, xs)) - average(xs) * average(ys)
        let sum2 = average(multiply(xs, xs)) - pow(average(xs), 2)
        
        let slope = sum1 / sum2
        let intercept = average(ys) - slope * average(xs)
    
        return ({ x in intercept + slope * x }, round(10*slope)/10)
    }
    
    /* Takes care Appends a ScrollableGraph instance to the public graphs array */
    func createGraph(_ graphTitle: String, _ yValues: [Double]) {
        let yValuesAverage = average(yValues)
        let (lobf, slope) = linearRegression(yValues)
        let dates: [Date] = tweets.map({$0.asDate()})
        
        let scrollableGraph = ScrollableGraph(graphTitle: graphTitle, x_data: dates, y_data: yValues, average: yValuesAverage, slope: slope, lobf: lobf)
        
        graphs.append(scrollableGraph)
    }
    
    func applyGraphData(graphView: ScrollableGraphView, graphNumber: Int) {
        let graph = graphs[graphNumber]
        
        graph.mainLinePlot = LinePlot(identifier: "\(graph.graphTitle)")
        graph.fitLinePlot = LinePlot(identifier: "FIT_\(graph.graphTitle)")
        
        styleGraph(linePlot: graph.mainLinePlot!, graphView: graphView)
        styleLOBF(linePlot: graph.fitLinePlot!, graphView: graphView, slope: graph.slope)
    }
    
    func getAverage(graphTitle: String) -> Double {
        for graph in graphs {
            if graphTitle == graph.graphTitle {
                return graph.average
            }
        }
        return 0
    }
    
    func getSlope(graphTitle: String) -> Double {
        for graph in graphs {
            if graphTitle == graph.graphTitle {
                return graph.slope
            }
        }
        return 0
    }
    
    func styleLOBF(linePlot: LinePlot, graphView: ScrollableGraphView, slope: Double){
        var lineColour = UIColor.white
        
        if (slope > 0) {
            lineColour = UIColor.green
        } else if (slope < 0) {
            lineColour = UIColor.red
        }
        
        // Setup the line plot.
        linePlot.lineWidth = 1.5
        linePlot.lineColor = lineColour
        linePlot.lineStyle = ScrollableGraphViewLineStyle.straight
        linePlot.shouldFill = false
        linePlot.adaptAnimationType = .easeOut
        linePlot.animationDuration = 1
       
        //Add the plot to the graph
        graphView.addPlot(plot: linePlot)
    }
    
    func styleGraph(linePlot: LinePlot, graphView: ScrollableGraphView){
        let colour1 = UIColor(named: "dysonRed")
        let colour2 = UIColor(named: "dysonDarkRed")
        let lineColour = UIColor.white
        
        // Setup the line plot.
        linePlot.lineWidth = 2
        linePlot.lineColor = colour2!
        linePlot.lineStyle = ScrollableGraphViewLineStyle.straight

        linePlot.shouldFill = true
        //linePlot.fillType = ScrollableGraphViewFillType.gradient
        linePlot.fillType = ScrollableGraphViewFillType.solid
        linePlot.fillColor = colour2!
        //linePlot.fillGradientType = ScrollableGraphViewGradientType.linear
        //linePlot.fillGradientStartColor = #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)
        //linePlot.fillGradientEndColor = colour1!
        
        linePlot.adaptAnimationType = ScrollableGraphViewAnimationType.easeOut
        linePlot.animationDuration = 1

        let dotPlot = DotPlot(identifier: "\(linePlot.identifier!)") // Add dots as well.
        dotPlot.dataPointSize = 3
        dotPlot.dataPointFillColor = lineColour

        dotPlot.adaptAnimationType = ScrollableGraphViewAnimationType.easeOut
        dotPlot.animationDuration = 1

        // Setup the reference lines.
        let referenceLines = ReferenceLines()

        referenceLines.referenceLineLabelFont = UIFont.boldSystemFont(ofSize: 10)
        referenceLines.referenceLineColor = lineColour.withAlphaComponent(0.5)
        referenceLines.referenceLineLabelColor = lineColour.withAlphaComponent(0.75)

        referenceLines.positionType = .relative
        // Reference lines will be shown at these values on the y-axis.
        referenceLines.includeMinMax = true
        referenceLines.dataPointLabelColor = lineColour.withAlphaComponent(0.75)

        // Setup the graph
        graphView.backgroundFillColor = colour1!
        graphView.dataPointSpacing = 50

        graphView.shouldAnimateOnStartup = true
        graphView.shouldAdaptRange = true
        graphView.shouldRangeAlwaysStartAtZero = true

        graphView.rangeMax = 750 //could use the max of the data for the plot? saves adapting range
        
        graphView.topMargin = 10
        graphView.bottomMargin = 10
        graphView.rightmostPointPadding = 0

        // Add everything to the graph.
        graphView.addReferenceLines(referenceLines: referenceLines)
        graphView.addPlot(plot: linePlot)
        graphView.addPlot(plot: dotPlot)
    }
    
}
