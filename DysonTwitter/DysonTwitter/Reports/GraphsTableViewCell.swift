//
//  GraphsTableViewCell.swift
//  DysonTwitter
//
//  Created by James Olrog on 13/02/2020.
//  Copyright © 2020 Will T P Alexander. All rights reserved.
//

import UIKit
import ScrollableGraphView

class GraphsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var graphTitle: UILabel!
    @IBOutlet weak var graphView: ScrollableGraphView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
