//
//  ScrollableGraph.swift
//  DysonTwitter
//
//  Created by James Olrog on 13/02/2020.
//  Copyright © 2020 Will T P Alexander. All rights reserved.
//

import Foundation
import ScrollableGraphView

// DO WE NEED REALLY TO STORE 'SLOPE' and 'AVERAGE'?
// Can be generated easily using provider methods
// average = scrollableGraphProvider?.average(graph.y_data)
// (~, slope) = scrollableGraphProvider?.linearRegression(graph.y_data)

class ScrollableGraph: ScrollableGraphViewDataSource {
    
    init(graphTitle: String, x_data: [Date], y_data: [Double], average: Double, slope: Double, lobf: @escaping (Double)->Double) {
    
        //Provided
        self.graphTitle = graphTitle
        self.x_data = x_data
        self.y_data = y_data
        self.average = average
        self.slope = slope
        self.lobf = lobf
        
        //Created
        self.mainLinePlot = nil
        self.fitLinePlot = nil
    }
    
    var graphTitle: String
    var x_data: [Date]
    var y_data: [Double]
    var average: Double
    var slope: Double
    var lobf: (Double)->Double
    
    var mainLinePlot: LinePlot?
    var fitLinePlot: LinePlot?
    
    /* Returns the y-value for each datapoint. Data is set for different plots by using the plot's identifer */
    func value(forPlot plot: Plot, atIndex pointIndex: Int) -> Double {
        if (plot.identifier == graphTitle) {
            return y_data[pointIndex]
        } else if (plot.identifier == "FIT_\(graphTitle)") {
            return lobf(Double(pointIndex))
        } else {
            //A default case to prevent fatal errors
            return 1.0
        }
    }
    
    /* Returns the x-label displayed for each datapoint */
    func label(atIndex pointIndex: Int) -> String {
        let time = x_data[pointIndex]
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM"
        let dateString = dateFormatterPrint.string(from: time)
        
        return dateString
    }

    /* Returns the number of discrete data locations to be plotted */
    func numberOfPoints() -> Int {
        return y_data.count
    }
}
