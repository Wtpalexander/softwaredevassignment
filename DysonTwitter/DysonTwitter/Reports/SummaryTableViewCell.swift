//
//  SummaryTableViewCell.swift
//  DysonTwitter
//
//  Created by James Olrog on 17/02/2020.
//  Copyright © 2020 Will T P Alexander. All rights reserved.
//

import UIKit

class SummaryTableViewCell: UITableViewCell {

    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var averageLabel: UILabel!
    @IBOutlet weak var averageValue: UILabel!
    @IBOutlet weak var slopeLabel: UILabel!
    @IBOutlet weak var slopeValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
