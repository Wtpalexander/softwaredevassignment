//
//  ReportViewController.swift
//  DysonTwitter
//
//  Created by Will T P Alexander on 29/01/2020.
//  Copyright © 2020 Will T P Alexander. All rights reserved.
//

import UIKit
import ScrollableGraphView

class ReportViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    lazy var scrollableGraphProvider: ScrollableGraphProvider? = nil
    
    //IB Outlets
    @IBOutlet weak var likesLabels: UILabel!
    @IBOutlet weak var retweetsAvLabel: UILabel!
    @IBOutlet weak var likesGradient: UILabel!
    @IBOutlet weak var retweetsGradient: UILabel!
    @IBOutlet weak var saveReportButton: UIButton!
    @IBOutlet weak var graphTableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    //Variables
    var username: String = "Username"
    var tweets: [Tweet] = []
    var graphCells: [GraphsTableViewCell] = []
    var selectedSegment: Int = 0
    var isSaveButtonHidden = false
    
    //References
    let defaults = UserDefaults.standard
    let decoder = JSONDecoder(); let encoder = JSONEncoder()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        graphTableView.delegate = self
        graphTableView.dataSource = self
        graphTableView.alwaysBounceVertical = false
        
        segmentedControl.addTarget(self, action: #selector(handleSegmentChange),
                                   for: .valueChanged)
        
        scrollableGraphProvider = ScrollableGraphProvider(tweets: tweets)
        setupGraphs()
        
        saveReportButton.isHidden = isSaveButtonHidden
    }
    
    @IBAction func saveReportTapped(_ sender: Any) {
        let newReport = Report(username: username, tweets: tweets, dateCreated: Date())
        if let data = defaults.data(forKey: "savedReports") {
            do {
                //If reports are present, decode them and append the new one and then re save them
                var decodedData = try decoder.decode([Report].self, from: data)
                decodedData.append(newReport)
                let dataToSave = try encoder.encode(decodedData)
                defaults.set(dataToSave, forKey:"savedReports")
                print("Report added to list")
                onSavedReport()
            } catch {
                print("Failed to decode and append JSON")
            }
        } else {
            //If there are no report present then encode the new report
            do{
                let dataToSave = try encoder.encode([newReport])
                defaults.set(dataToSave, forKey:"savedReports")
                print("First report added")
                onSavedReport()
            } catch {
                print("Failed to add first report")
            }
        }
   }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      switch selectedSegment {
        case 0:
            return 265
        case 1:
            return 108
        default:
            return 50
      }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scrollableGraphProvider?.graphs.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch selectedSegment {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "GraphsTableViewCell", for: indexPath) as? GraphsTableViewCell else {
                fatalError("Cell not available")
            }
            
            if let graphIdentifier = cell.graphView.plots.first?.identifier {
                if let graph = scrollableGraphProvider?.graphs.first(where:{$0.graphTitle == graphIdentifier}) {
                
                    cell.graphView.dataSource = graph
                    cell.graphTitle.text = graph.graphTitle

                    //Only needs to be run once!
                    if (graph.mainLinePlot == nil && graph.fitLinePlot == nil) {
                       scrollableGraphProvider?.applyGraphData(graphView: cell.graphView, graphNumber: indexPath.row)
                    }
                }
            } else {
                let graph = scrollableGraphProvider?.graphs[indexPath.row]
                cell.graphView.dataSource = graph
                cell.graphTitle.text = graph?.graphTitle

                //Only needs to be run once!
                if (graph?.mainLinePlot == nil && graph?.fitLinePlot == nil) {
                  scrollableGraphProvider?.applyGraphData(graphView: cell.graphView, graphNumber: indexPath.row)
                }
            }
            
            //print(graph?.graphTitle)
            //print(cell.graphView.plots[0].identifier)
            
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SummaryTableViewCell", for: indexPath) as? SummaryTableViewCell else {
                fatalError("Cell not available")
            }
            
            let graph = scrollableGraphProvider!.graphs[indexPath.row]
            let title = graph.graphTitle
            
            cell.cellTitle.text = title
            cell.averageLabel.text = "\(title) Average:"
            cell.averageValue.text = "\(scrollableGraphProvider?.getAverage(graphTitle: title) ?? 0.0)"
            cell.slopeLabel.text = "\(title) Slope:"
            cell.slopeValue.text = "\(scrollableGraphProvider?.getSlope(graphTitle: title) ?? 0.0)"
            
            if indexPath.row % 2 != 0 {
                cell.contentView.backgroundColor = UIColor(named: "dysonDarkRed")
            } else {
                cell.contentView.backgroundColor = UIColor(named: "dysonRed")
            }
            
            return cell
            
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SummaryTableViewCell", for: indexPath) as? SummaryTableViewCell else {
                fatalError("Cell not available")
            }
            return cell
        }
    }
    
    func onSavedReport() {
        let alert = UIAlertController(title: "Success!", message: "Report has been saved successfully", preferredStyle: UIAlertController.Style.alert)
        let continueAction = UIAlertAction(title: "Continue", style: UIAlertAction.Style.default) {
             UIAlertAction in
            self.dismiss(animated: true, completion: nil)
         }
        alert.addAction(continueAction)
        self.present(alert, animated: true, completion: nil)
    }

    @objc fileprivate func handleSegmentChange() {
        graphTableView.reloadData()
        //graphTableView.setContentOffset(.zero, animated: false)
        graphTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        selectedSegment = segmentedControl.selectedSegmentIndex
        //graphTableView.reloadData()
    }
    
    func setupGraphs() {
        let likes = tweets.map({Double($0.favorite_count)})
        let retweets = tweets.map({Double($0.retweet_count)})
        //let comments = tweets.map({Double($0.comments)})
        //let engagement = tweets.map({Double($0.engagement)})
        
        //TODO: Fix this enaggement metric
        scrollableGraphProvider!.createGraph("Engagement", retweets)
        scrollableGraphProvider!.createGraph("Likes", likes)
        scrollableGraphProvider!.createGraph("Retweets", retweets)
//        scrollableGraphProvider!.createGraph("Comments", likes)
        
    }
}
