//
//  ViewController.swift
//  DysonTwitter
//
//  Created by Will T P Alexander on 29/01/2020.
//  Copyright © 2020 Will T P Alexander. All rights reserved.
//

import UIKit

enum Role {
    case Marketting
    case Software
    case Management
}

struct User {
    var username: String
    var password: String
    var role: Role
}

class LogInViewController: UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!

    let users: [User] = [User(username: "Nic", password: "123456", role: Role.Marketting),
                         User(username: "James", password: "123456", role: Role.Software),
                         User(username: "Freddie", password: "123456", role: Role.Management)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        //Round the log in button corners
        loginButton.layer.cornerRadius = 17.5
        
        //Set the username view as the first responder
        usernameTextField.becomeFirstResponder()
    }

    @IBAction func loginTapped(_ sender: Any) {
        let username = usernameTextField.text
        let password = passwordTextField.text
        
        for user in users {
            if user.username == username && user.password == password {
                switch user.role {
                case .Marketting:
                    self.performSegue(withIdentifier: "toMarketingViewController", sender: nil)
                case .Software:
                    self.performSegue(withIdentifier: "goToSoftwareViewController", sender: nil)
                case .Management:
                    self.performSegue(withIdentifier: "toManagementViewController", sender: nil)
                }
            }
        }
        
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
