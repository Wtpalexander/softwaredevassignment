//
//  SoftwareViewController.swift
//  DysonTwitter
//
//  Created by Will T P Alexander on 29/01/2020.
//  Copyright © 2020 Will T P Alexander. All rights reserved.
//

import UIKit

class SoftwareViewController: UIViewController {
    
    @IBOutlet weak var crashesBackgroundView: UIView!
    @IBOutlet weak var reviewsBackgroundView: UIView!
    @IBOutlet weak var starsBackgroundView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        crashesBackgroundView.layer.cornerRadius = crashesBackgroundView.frame.width/10
        reviewsBackgroundView.layer.cornerRadius = reviewsBackgroundView.frame.width/10
        starsBackgroundView.layer.cornerRadius = starsBackgroundView.frame.width/10
    }
    
}

extension SoftwareViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "LogTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? LogTableViewCell else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        
        cell.timeLabel.text = "12:00"
        cell.logLabel.text = "Test Log"

        return cell
    }
}
